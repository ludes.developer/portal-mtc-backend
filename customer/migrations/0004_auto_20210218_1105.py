# Generated by Django 3.1.6 on 2021-02-18 04:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0003_auto_20210218_1103'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='date_generated',
            field=models.TextField(blank=True, default='2021-02-18 11:05:37'),
        ),
    ]
