from datetime import datetime
from django.db import models

today = datetime.today().strftime('%Y-%m-%d %H:%M:%S')


class Customer(models.Model):
    customer_name = models.TextField(blank=True, null=True)
    device_name = models.TextField(blank=True, null=True)
    model = models.TextField(blank=True, null=True)
    card = models.TextField(blank=True, null=True)
    sn = models.TextField(blank=True, null=True)
    version = models.TextField(blank=True, null=True)
    # date_generated = models.DateTimeField(auto_now_add=True)
    date_generated = models.TextField(blank=True, null=True)
    generated_by = models.TextField(blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.customer_name, self.device_name)
