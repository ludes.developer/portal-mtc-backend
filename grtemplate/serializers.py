from datetime import datetime
from rest_framework import serializers
from grtemplate.models import Grtemplate


class GrtemplateSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField(read_only=True)
    # username = serializers.CharField(max_length=100)
    class Meta:
        model = Grtemplate
        fields = ['id', 'script_name', 'device_pid']

    def create(self, validated_data):
        """
        Create and return a new `Admin` instance, given the validated data.
        """
        return Grtemplate.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Admin` instance, given the validated data.
        """
        instance.script_name = validated_data.get(
            'script_name', instance.script_name)
        instance.device_name = validated_data.get(
            'device_pid', instance.device_pid)
        instance.save()
        return instance