from django.contrib import admin
from .models import Grtemplate


class GrtemplateAdmin(admin.ModelAdmin):
    pass


admin.site.register(Grtemplate, GrtemplateAdmin)