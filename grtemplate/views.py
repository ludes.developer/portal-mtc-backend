from grtemplate.models import Grtemplate
from grtemplate.serializers import GrtemplateSerializer
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.parsers import JSONParser


class GrtemplateCreateRead(generics.ListCreateAPIView):
    queryset = Grtemplate.objects.all().order_by('id').reverse()
    serializer_class = GrtemplateSerializer

    def perform_create(self, serializer):
        print(self.request.data["device_pid"])
        queryset = Grtemplate.objects.filter(
            device_pid=self.request.data["device_pid"])
        if queryset.exists():
            raise ValidationError('Duplicate Detected !!!')
        serializer.save()


class GrtemplateUpdateDelete(generics.RetrieveUpdateDestroyAPIView):
    queryset = Grtemplate.objects.all()
    serializer_class = GrtemplateSerializer


@api_view(['POST'])
def grtemplate_delete_selected(request):
    request_data = JSONParser().parse(request)
    request_data = request_data["dataGRTemplate"]
    for data in request_data:
        grtemplate = Grtemplate.objects.get(id=data["id"])
        grtemplate.delete()
    # print(request_data)
    return Response(request_data)