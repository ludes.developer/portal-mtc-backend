from datetime import datetime
from django.db import models

today = datetime.today().strftime('%Y-%m-%d %H:%M:%S')


class Software(models.Model):
    software_version = models.TextField(blank=True, null=True)
    eos_maintenance = models.TextField(blank=True, null=True)
    last_date_of_support = models.TextField(blank=True, null=True)
    technology = models.TextField(blank=True, null=True)
    date_generated = models.TextField(blank=True, null=True)
    generated_by = models.TextField(blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.software_version, self.eos_maintenance)


class SoftwareApproval(models.Model):
    software_version = models.TextField(blank=True, null=True)
    eos_maintenance = models.TextField(blank=True, null=True)
    last_date_of_support = models.TextField(blank=True, null=True)
    technology = models.TextField(blank=True, null=True)
    date_generated = models.TextField(blank=True, null=True)
    generated_by = models.TextField(blank=True, null=True)
    approval_id = models.IntegerField()

    def __str__(self):
        return '%s %s' % (self.software_version, self.eos_maintenance)
