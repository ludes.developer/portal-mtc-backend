from django.contrib import admin
from .models import Software, SoftwareApproval


class SoftwareAdmin(admin.ModelAdmin):
    pass


admin.site.register(Software, SoftwareAdmin)


class SoftwareApprovalAdmin(admin.ModelAdmin):
    pass


admin.site.register(SoftwareApproval, SoftwareApprovalAdmin)
