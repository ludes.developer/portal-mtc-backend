from django.urls import path
from software import views

urlpatterns = [
    path('api/v1/portal/software/', views.SoftwareCreateRead.as_view()),
    path('api/v1/portal/software/<int:pk>/',
         views.SoftwareUpdateDelete.as_view()),
    path('api/v1/portal/software/insert_selected/',
         views.software_insert_selected),
    path('api/v1/portal/software/update_selected/',
         views.software_update_selected),
    path('api/v1/portal/software/delete_selected/',
         views.software_delete_selected),
    path('api/v1/portal/software_approval/',
         views.SoftwareApprovalCreateRead.as_view()),
    path('api/v1/portal/software_approval/member/',
         views.software_approval_member),
    path('api/v1/portal/software_approval/<int:pk>/',
         views.SoftwareApprovalUpdateDelete.as_view()),
    path('api/v1/portal/software_approval/delete_selected/',
         views.software_approval_delete_selected),
]
