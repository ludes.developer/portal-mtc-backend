from django.urls import path
from log import views

urlpatterns = [
    path('api/v1/portal/log/', views.LogCreateRead.as_view()),
    path('api/v1/portal/log/<int:pk>/',
         views.LogUpdateDelete.as_view()),
    path('api/v1/portal/log/insert_selected/',
         views.log_insert_selected),
    path('api/v1/portal/log/update_selected/',
         views.log_update_selected),
    path('api/v1/portal/log/delete_selected/',
         views.log_delete_selected),
    path('api/v1/portal/log_approval/', views.LogApprovalCreateRead.as_view()),
    path('api/v1/portal/log_approval/member/', views.log_approval_member),
    path('api/v1/portal/log_approval/<int:pk>/',
         views.LogApprovalUpdateDelete.as_view()),
    path('api/v1/portal/log_approval/delete_selected/',
         views.log_approval_delete_selected),
]
