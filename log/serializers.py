from datetime import datetime
from rest_framework import serializers
from log.models import Log, LogApproval


class LogSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField(read_only=True)
    # username = serializers.CharField(max_length=100)
    class Meta:
        model = Log
        fields = ['id', 'name_log', 'explanation',
                  'workaround', 'technology', 'date_generated', 'generated_by'
                  ]

    def create(self, validated_data):
        """
        Create and return a new `Admin` instance, given the validated data.
        """
        return Log.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Admin` instance, given the validated data.
        """
        instance.name_log = validated_data.get(
            'name_log', instance.name_log)
        instance.explanation = validated_data.get(
            'explanation', instance.explanation)
        instance.workaround = validated_data.get(
            'workaround', instance.workaround)
        instance.technology = validated_data.get(
            'technology', instance.technology)
        instance.date_generated = validated_data.get(
            'date_generated', instance.date_generated)
        instance.generated_by = validated_data.get(
            'generated_by', instance.generated_by)
        instance.save()
        return instance


class LogApprovalSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogApproval
        fields = ['id', 'name_log', 'explanation',
                  'workaround', 'technology', 'date_generated', 'generated_by', 'approval_id'
                  ]

    def create(self, validated_data):
        """
        Create and return a new `Admin` instance, given the validated data.
        """
        return LogApproval.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Admin` instance, given the validated data.
        """
        instance.name_log = validated_data.get(
            'name_log', instance.name_log)
        instance.explanation = validated_data.get(
            'explanation', instance.explanation)
        instance.workaround = validated_data.get(
            'workaround', instance.workaround)
        instance.technology = validated_data.get(
            'technology', instance.technology)
        instance.date_generated = validated_data.get(
            'date_generated', instance.date_generated)
        instance.generated_by = validated_data.get(
            'generated_by', instance.generated_by)
        instance.approval_id = validated_data.get(
            'approval_id', instance.approval_id)
        instance.save()
        return instance
