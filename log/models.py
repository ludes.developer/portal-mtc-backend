from datetime import datetime
from django.db import models

today = datetime.today().strftime('%Y-%m-%d %H:%M:%S')

class Log(models.Model):
    name_log = models.TextField(blank=True, null=True)
    explanation = models.TextField(blank=True, null=True)
    workaround = models.TextField(blank=True, null=True)
    technology = models.TextField(blank=True, null=True)
    date_generated = models.TextField(blank=True, null=True)
    generated_by = models.TextField(blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.name_log, self.explanation)


class LogApproval(models.Model):
    name_log = models.TextField(blank=True, null=True)
    explanation = models.TextField(blank=True, null=True)
    workaround = models.TextField(blank=True, null=True)
    technology = models.TextField(blank=True, null=True)
    date_generated = models.TextField(blank=True, null=True)
    generated_by = models.TextField(blank=True, null=True)
    approval_id = models.IntegerField()

    def __str__(self):
        return '%s %s' % (self.name_log, self.explanation)
