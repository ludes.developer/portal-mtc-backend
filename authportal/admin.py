from django.contrib import admin
from .models import ReqAdmin, Admin


class AdminAdmin(admin.ModelAdmin):
    pass


admin.site.register(Admin, AdminAdmin)


class ReqAdminAdmin(admin.ModelAdmin):
    pass


admin.site.register(ReqAdmin, ReqAdminAdmin)
