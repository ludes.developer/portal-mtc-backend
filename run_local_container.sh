#!/bin/bash

# run compose for portal-mtc-backend
docker-compose up -d;

secs=$((45))
while [ $secs -gt 0 ]; do
   echo -ne "$secs\033[0K\r"
   sleep 1
   : $((secs--))
done

# setup mysql database db_portal
docker cp ./create_db_portal.sql portal_db_local:/
docker exec portal_db_local /bin/sh -c 'mysql -h localhost -u root -pN3t0prmgr! </create_db_portal.sql'

secs=$((10))
while [ $secs -gt 0 ]; do
   echo -ne "$secs\033[0K\r"
   sleep 1
   : $((secs--))
done

# make migration and migrate django db_portal
docker-compose run --rm be python3 manage.py makemigrations;
docker-compose run --rm be python3 manage.py migrate;
docker restart portal_be_local;
