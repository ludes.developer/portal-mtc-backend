from django.contrib import admin
from .models import Hardware, HardwareApproval


class HardwareAdmin(admin.ModelAdmin):
    pass


admin.site.register(Hardware, HardwareAdmin)


class HardwareApprovalAdmin(admin.ModelAdmin):
    pass


admin.site.register(HardwareApproval, HardwareApprovalAdmin)
