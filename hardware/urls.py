from django.urls import path
from hardware import views

urlpatterns = [
    path('api/v1/portal/hardware/', views.HardwareCreateRead.as_view()),
    path('api/v1/portal/hardware/<int:pk>/',
         views.HardwareUpdateDelete.as_view()),
    path('api/v1/portal/hardware/insert_selected/',
         views.hardware_insert_selected),
    path('api/v1/portal/hardware/update_selected/',
         views.hardware_update_selected),
    path('api/v1/portal/hardware/delete_selected/',
         views.hardware_delete_selected),
    path('api/v1/portal/hardware_approval/',
         views.HardwareApprovalCreateRead.as_view()),
    path('api/v1/portal/hardware_approval/member/',
         views.hardware_approval_member),
    path('api/v1/portal/hardware_approval/<int:pk>/',
         views.HardwareApprovalUpdateDelete.as_view()),
    path('api/v1/portal/hardware_approval/delete_selected/',
         views.hardware_approval_delete_selected),
]
