# Generated by Django 3.1.6 on 2021-03-09 08:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hardware', '0024_auto_20210226_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hardware',
            name='date_generated',
            field=models.TextField(blank=True, default='2021-03-09 15:58:40'),
        ),
        migrations.AlterField(
            model_name='hardwareapproval',
            name='date_generated',
            field=models.TextField(blank=True, default='2021-03-09 15:58:40'),
        ),
    ]
